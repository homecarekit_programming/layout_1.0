package com.example.project_haha;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 김도영 on 2017-11-07.
 */

public class Refer_list_adapter extends BaseAdapter{
    ArrayList<Refer_item> refer_item = new ArrayList<Refer_item>();

    @Override
    public int getCount(){
        return refer_item.size();
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return refer_item.get(position) ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final int pos = position;
        final Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.refer_item, parent, false);
        }

        ImageView iconImageView = (ImageView) convertView.findViewById(R.id.listicon) ;
        TextView name = (TextView) convertView.findViewById(R.id.username) ;
        TextView date = (TextView) convertView.findViewById(R.id.date) ;

        Refer_item listViewItem = refer_item.get(position);

        iconImageView.setImageDrawable(listViewItem.getIcon());
        name.setText(listViewItem.getName());
        date.setText(listViewItem.getDate());

        return convertView;
    }

    public void addItem(Drawable icon, String name, String date) {
        Refer_item item = new Refer_item();

        item.setIcon(icon);
        item.setName(name);
        item.setDate(date);

        refer_item.add(item);
    }




}
