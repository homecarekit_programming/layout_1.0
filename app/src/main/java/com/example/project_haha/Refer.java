package com.example.project_haha;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Refer extends AppCompatActivity {

    long time = System.currentTimeMillis();
    Date date = new Date(time);
    Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer);
        i = new Intent(this, Refer_Graph.class);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd\nHH:mm");
        String getTime = sdf.format(date);

        ListView listview;
        Refer_list_adapter adapter = new Refer_list_adapter();

        listview = (ListView) findViewById(R.id.ReferList);
        listview.setAdapter(adapter);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.icon),
                "김도영", getTime);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.icon),
                "김동수", getTime);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.icon),
                "김동현", getTime);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.icon),
                "김동겸", getTime);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // get item
                Refer_item item = (Refer_item) parent.getItemAtPosition(position);
                i.putExtra("INPUT_NAME", item.getName());
                i.putExtra("INPUT_DATE", item.getDate());
                startActivity(i);
                String name = item.getName();
                String date = item.getDate();
                Drawable iconDrawable = item.getIcon();
            }
        });
    }
}