package com.example.project_haha;

import android.graphics.drawable.Drawable;

/**
 * Created by 김도영 on 2017-11-07.
 */

public class Refer_item {

    private Drawable icon;
    private String name,date;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
