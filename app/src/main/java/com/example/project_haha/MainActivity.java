package com.example.project_haha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickCheck(View v)
    {
        Intent i = new Intent(this,Check.class);
        startActivity(i);
    }

    public void onClickRefer(View v)
    {
        Intent i = new Intent(this,Refer.class);
        startActivity(i);
    }

}
